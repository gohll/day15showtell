(function () {
    angular
        .module("TodoApp", [])
        .controller("AddTodoCtrl", AddTodoCtrl)
        .controller("ListTodoCtrl", ListTodoCtrl);

    AddTodoCtrl.$inject = ["$http", "dbService"];
    ListTodoCtrl.$inject = ["$http", "dbService"];

    function AddTodoCtrl($http, dbService) {
        var vm = this;
        vm.task = dbService.getTask();
        vm.status = {
            message: "",
            code: 0
        };

        vm.addTodo = function () {
            //sends the vm.task object to dbService.save service
            dbService.save(vm.task)
                //executed according to the defer.promise that was returned in the dbService function(...
                //    used by service
                .then(function (result) {
                    vm.status.message = "New task inserted successfully.";
                    vm.status.code = 202;
                    vm.task = dbService.getTask();
                })
                .catch(function (err) {
                    console.log(err);
                    vm.status.message = "An error has occurred. Addition not successful";
                    vm.status.code = 400;
                });
        };
    };


    
    function ListTodoCtrl($http, dbService) {
        var vm = this;
        
        vm.getTasks = function() {
            return dbService.tasks
        };
        
        // vm.getCompleteTodos = function() {
        //     return dbService.completedTasks
        // }
        // vm.isComplete = function(item) {
        //     return item.completed;
        // };
        
        vm.status = {
            message1: "",
            code1: 0,
            message2: "",
            code2: 0
        };

        dbService.list()
            .then(function (results) {
               if (results){
                vm.status.message1 = "Check task to update status as completed.";
                vm.status.code1 = 202 
               };
            }).catch(function (err) {
            console.log(err);
            vm.status.message1 = "An error has occurred. Unable to provide listing";
            vm.status.code1 = 400;
        });

        vm.updateCompleted = function (t) {
           t.completed = 1; //?
            //sends the vm.task object to dbService.update service
            console.log(t)
            dbService.update(t)
                .then(function (result) {
                    vm.status.message1 = "Completed task updated successfully.";
                    vm.status.code1 = 202;
                    console.info(result);
                }).catch(function (err) {
                console.log(err);
                vm.status.message1 = "An error has occurred.";
                vm.status.code1 = 400;
            });
        };

        vm.deleteCompleted = function (t) {
            //sends the vm.task object to dbService.delete service
            dbService.delete(t)
                .then(function (result) {
                    vm.status.message2 = "Completed task removed successfully.";
                    vm.status.code2 = 202;
                    console.info(result);
                }).catch(function (err) {
                console.log(err);
                vm.status.message2 = "An error has occurred.";
                vm.status.code2 = 400;
            });
        };
    };
})();

