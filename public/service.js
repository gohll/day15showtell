(function () {
    angular.module("TodoApp")
        .service("dbService", dbService);

    dbService.$inject = ["$http", "$q"];

    function dbService($http, $q) {
        var service = this;
        var todate = new Date();

        service.tasks = [];
        // service.completedTasks = [];

        service.getTask = function () {
            var task = {};
            task.taskname = "";
            task.priority = "";
            task.completed = 0;
            task.duedate = "";
            task.createdate = todate;

            return task;
        };

        service.save = function (task) {
            var defer = $q.defer();
            $http.post("api/task/save", task)
                .then(function (result) {
                    console.log(task)
        //includes the new addition to the coomon list array in the ListTodo controller
                    service.tasks.push({
                        "task_name": task.taskname,
                        "priority": task.priority,
                        "completed": task.completed,
                        "due_date":task.duedate,
                        "create_date": task.createdate,
                        "id":result.data.id
                    })
                    defer.resolve(result.data);
                })
                .catch(function (error) {
                    defer.reject(error.status);
                });
            return defer.promise;
        };

        service.list = function () {
            var defer = $q.defer();
            $http.get("api/task/list")
                .then(function (results) {
                    service.tasks = results.data;
                    defer.resolve(results.data);
                })
                .catch(function (error) {
                    defer.reject(error.status);
                });
            return defer.promise;
        };

        service.update = function (t) {
            var defer = $q.defer();
            $http.get("api/task/update", {
                     params: {
                        id: t.id
                    }
                })
                .then(function (result) {
            // service.completedTasks = results.data;
            //  not used. Used filter for services.tasks io get completed list using filter
                    defer.resolve(result.data);
                })
                .catch(function (error) {
                    //  puts the HTTP status into the defer object
                    defer.reject(error.status);
                });
            return defer.promise;
        }

        service.delete = function (t) {
            console.log(t)
            var defer = $q.defer();
            $http.put("api/task/delete", {id: t.id})
                .then(function (result) {
                    // find the task with id = t.id in the array and splice it
                    for(var i in service.tasks) {
                        if(service.tasks[i].id == t.id) {
                            service.tasks.splice(i, 1);
                        }
                    }
                    defer.resolve(result.data);
                })
                .catch(function (error) {
                    //  puts the HTTP status into the defer object
                    defer.reject(error.status);
                });

            return defer.promise;
        }
    }

})();


