var Task = require("./task");

exports.saveTask = function (req, res) {
  
     var dueDate = req.body.duedate.substring(0, req.body.duedate.indexOf('T'));
     var createDate = req.body.createdate.substring(0, req.body.createdate.indexOf('T'));
    
     var task = new Task(
            req.body.taskname,
            req.body.priority,
            req.body.completed,
            dueDate,
            createDate
        );
    
        task.save(function (err, result) {
                if (err) {
                        res.status(500).end();
                            return console.log("Some errors occurred", err);
                    }
                console.log("Saved task successfully");
                res.status(202).json({id: result.insertId});
            });
};

exports.listTask = function (req, res) {

    var tasks = [];
    Task.list(function (err, results) {
        if (err) {
            res.status(500).end();
            return console.log("Some errors occured", err);
        }
        console.log("Tasks for update");
        res.status(202).json( results);
    });
};

exports.updateTask = function (req, res) {
    Task.markAsComplete(req.query.id, function (err, result) {
        if (err) {
            res.status(500).end();
            return console.log("Some errors occurred", err);
        }
        console.log("Task updated successfully");
        res.status(202).json({id: result.updateId});
    })
};

// exports.listCompletedTask = function (req, res) {
//
//     // var tasks = [];
//     Task.listCompleted(function (err, results) {
//         if (err) {
//             res.status(500).end();
//             return console.log("Some errors occured", err);
//         }
//         console.log("Completed tasks for removal");
//         res.status(202).json( results);
//     });
// };

exports.deleteTask = function (req, res) {
    Task.delete(req.body.id, function (err, result) {
        if (err) {
            res.status(500).end();
            return console.log("Some errors occurred", err);
        }
        console.log("Task deleted successfully");
        res.status(202).json({url: "/api/task/delete" + result.deleteId});
    })
};
       