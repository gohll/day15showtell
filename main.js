//Load express
var express = require("express");
var routes = require("./routes");
var bodyParser = require("body-parser");

var app = express();

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

app.post("/api/task/save", routes.saveTask);
app.get("/api/task/list", routes.listTask);
app.get("/api/task/update", routes.updateTask);
// app.get("/api/task/listCompleted", routes.listCompletedTask);
app.put("/api/task/delete", routes.deleteTask);

// Serves files from public directory, __dirname being absolute path
app.use(express.static(__dirname + "/public"));
app.use( express.static(__dirname + "/public/bower_components"));

//Start the web server on port 3000 unless specified
var portNumber = process.argv[2] || 3000;
console.info(process.argv[2]);

app.listen(parseInt(portNumber), function(){
    console.info("Webserver started on port %s", portNumber);
});
