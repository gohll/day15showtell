var mysql = require("mysql");

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "laylan",
    password: "asuwish8",
    database: "employees",
    connectionLimit: 4
});

const INSERTSQL = "insert into task " +
    "(task_name, priority, completed, due_date, create_date) values (?,?,?,?,?)";

const LISTSQL = "select * from task";
const UPDATESQL = "update task set completed = 1 where id = ?";
// const LISTCOMPLETEDSQL = "select * from task where completed = 1 ";
const DELETESQL = "delete from task where id = ?";


var Task = function (taskName, priority, completed, dueDate, createDate) {
    this.task_name = tasName;
    this.priority = priority;
    this.completed = completed;
    this.dueDate = dueDate;
    this.createdate = createDate;
};



Task.prototype.save = function (callback) {
    var task = this;
    
    pool.getConnection(function (err, connection) {
        if (err) {
            
            return callback(err);
        }
        var values = [task.task_name, task.priority, task.completed, task.dueDate, task.createdate];
        connection.query(INSERTSQL, values, function (err, result) {
            connection.release();
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};

Task.list = function (callback) {
    var tasks = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(LISTSQL, [], function (err, result) {
            connection.release();
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};

// Task.listCompletedTask = function (callback) {
//     var task = this;
//     pool.getConnection(function (err, connection) {
//         if (err) {
//             return callback(err);
//         }
//         connection.query(LISTCOMPLETEDSQL, [], function (err, result) {
//             connection.release();
//             if (err) {
//                 return callback(err);
//             }
//             callback(null, result);
//         });
//     });
// };

Task.update = function (callback) {
    var task = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(UPDATESQL, [param.req.id], function (err, result) {
            connection.release();
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};

Task.delete = function (id, callback) {
    var task = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(DELETESQL, [id], function (err, result) {
            connection.release();
            if (err) {
                return callback(err);
            }
            callback(null, result);
        });
    });
};

module.exports = Task;
